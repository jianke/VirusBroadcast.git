# VirusBroadcast

#### 介绍
一个用 Java 编写的病毒传播模拟器

源码来自 https://github.com/KikiLetGo/VirusBroadcast

#### 软件截图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0205/173038_81745d81_364.png "屏幕截图.png")


#### 安装教程

1.  git clone https://gitee.com/jianke/VirusBroadcast.git
2.  cd src
3.  javac *.java
4.  java -cp . Main